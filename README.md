# FontAwesome pour SPIP

Tout le jeu d'icone gratuit (cf https://fontawesome.com/plans) disponible facilement au format SVG,
en sprite pour une utilisation intensive ou a l'unité

Voir une démo sur ?page=demo/fontawesome après installation du plugin dans SPIP

## Utilisation

Dans les squelettes, le plugin permet d'insérer facilement des icones FontAwesome

### Utilisation en sprite

Les sprites sont intéressantes si vous faites une utilisation intensive du jeu d'icone et si vos visiteurs reviennent souvent
(ex: un espace d'administration).
Dans ce cas toutes les icones sont chargées en une seule fois dans le cache navigateur et s'affichent rapidement à chaque utilisation.

Syntaxe :
`#FA_ICON{name,class,alt}`

Exemples :
```
    #FA_ICON{beer,'',Beer}
    #FA_ICON{brands#ubuntu,icon-sm,Ubuntu}
    #FA_ICON{regular#kiss,icon-lg,Kiss}
```

### Utilisation à l'unité

L'utilisation à l'unité est plus adaptée pour un usage ponctuel sur un site public.
Les icones SVG seront insérées une par une au besoin dans le code HTML de la page

```
    #FA_ICON{img/fa/solid/beer.svg,'',Beer}
    #FA_ICON{img/fa/brands/ubuntu.svg,icon-sm,Ubuntu}
    #FA_ICON{img/fa/regular/kiss.svg,icon-lg,Kiss}
```
