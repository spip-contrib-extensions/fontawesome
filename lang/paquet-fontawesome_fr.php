<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'fontawesome_description' => 'Tout le jeu d\'icône Font Awesome en version gratuite prêt à l\'emploi en SVG spritée.
Voir ?page=demo/fontawesome',
	'fontawesome_nom' => 'Font Awesome',
	'fontawesome_slogan' => 'La version free de Font Awesome en SVG',
);
